﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getUsman : MonoBehaviour {

	public GameObject usmanFace;
	public GameObject usmanBackground;
	public GameObject usmanRectangle;

	public GameObject medvedFace;
	public GameObject sunMedved;
	public GameObject zaborMedved;
	public GameObject cloudMedved;
	public GameObject rectangleMedved;




	public GameObject putinFace;
	public GameObject putinRectangle;

	public void usman()
	{
		Invoke ("levelUsman", 1);
	}

	public void levelUsman()
	{
		usmanFace.SetActive (true);
		usmanBackground.SetActive (true);
		usmanRectangle.SetActive (true);

		medvedFace.SetActive (false);
		sunMedved.SetActive (false);
		zaborMedved.SetActive (false);
		cloudMedved.SetActive (false);
		rectangleMedved.SetActive (false);

		putinFace.SetActive (false);
		putinRectangle.SetActive (false);
	}

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this);
		}
	
	// Update is called once per frame
	void Update () {
		
	}
}
