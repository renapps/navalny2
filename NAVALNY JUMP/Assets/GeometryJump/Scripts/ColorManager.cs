﻿/***********************************************************************************************************
 * Produced by App Advisory - http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System.Collections;
#if AADOTWEEN
using DG.Tweening;
#endif

/// <summary>
/// Class in charge to manage the background colors
/// </summary>
namespace AppAdvisory.GeometryJump
{
	public class ColorManager : MonoBehaviorHelper 
	{
		public Color[] colors;

		public float timeChangeColor = 10;

		bool isGameOver = false;

		public GameObject usmanFace;
		public GameObject usmanBackground;
		public GameObject usmanRectangle;

		public GameObject medvedFace;
		public GameObject sunMedved;
		public GameObject zaborMedved;
		public GameObject cloudMedved;
		public GameObject rectangleMedved;



		public GameObject putinFace;
		public GameObject putinRectangle;


		public void levelUsman()
		{
			usmanFace.SetActive (true);
			usmanBackground.SetActive (true);
			usmanRectangle.SetActive (true);

			medvedFace.SetActive (false);
			sunMedved.SetActive (false);
			zaborMedved.SetActive (false);
			cloudMedved.SetActive (false);
			rectangleMedved.SetActive (false);

			putinFace.SetActive (false);
			putinRectangle.SetActive (false);
		}

		public void levelMedved ()
		{
			usmanFace.SetActive (false);
			usmanBackground.SetActive (false);
			usmanRectangle.SetActive (false);

			medvedFace.SetActive (true);
			sunMedved.SetActive (true);
			zaborMedved.SetActive (true);
			cloudMedved.SetActive (true);
			rectangleMedved.SetActive (true);

			putinFace.SetActive (false);
			putinRectangle.SetActive (false);
		}

		public void levelPutin ()
		{
			usmanFace.SetActive (false);
			usmanBackground.SetActive (false);
			usmanRectangle.SetActive (false);

			medvedFace.SetActive (false);
			sunMedved.SetActive (false);
			zaborMedved.SetActive (false);
			cloudMedved.SetActive (false);
			rectangleMedved.SetActive (false);

			putinFace.SetActive (true);
			putinRectangle.SetActive (true);
		}


	


		void Awake()
		{
			Color c = PlayerPrefsX.GetColor("BACKGROUND_COLOR", colors[0]);
			cam.backgroundColor = c;
		}

		void OnEnable()
		{
			GameManager.OnGameStart += OnGameStart;

			GameManager.OnGameOverStarted += OnGameOverStarted;

			Color c = PlayerPrefsX.GetColor("BACKGROUND_COLOR", colors[0]);
			cam.backgroundColor = c;
		}

		void OnDisable()
		{
			GameManager.OnGameStart -= OnGameStart;

			GameManager.OnGameOverStarted -= OnGameOverStarted;

		}

		void OnGameStart()
		{
			isGameOver = false;
			ChangeColor();
		}

		void OnGameOverStarted()
		{
			isGameOver = true;

		}

		void ChangeColor(){


			Color colorTemp = colors [UnityEngine.Random.Range (0, colors.Length)];

			#if AADOTWEEN
			cam.DOColor(colorTemp,3f).SetEase(Ease.Linear)
				.SetDelay(timeChangeColor)
				.OnComplete(() => {
					PlayerPrefsX.SetColor("BACKGROUND_COLOR", cam.backgroundColor);

					if(!isGameOver)
					{
						ChangeColor();
					}
				});
			#endif

		}

		void OnApplicationQuit()
		{
			PlayerPrefs.Save();
		}
	}
}